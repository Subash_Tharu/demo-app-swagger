package com.oncore.demoappswagger.service;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

	// corn expression
	// Every friday at 2:00 pm send mails
	@Scheduled(cron = "0 0 12 ? * FRI")
	//@Scheduled(fixedDelay = 5000)
	public void sendMailNotification() {

		System.out.println("sending mail .....");

		String from = "dev.oncorecorporation@gmail.com";
		String to = "pramish.thapa17@gmail.com";
		String subject = "Remainder: OTL and weekly report";

		String message = "Please !, Fill up the oracel time sheet and upated the weekly report by EOD. "
				+ "If done Kindly igone this. Thank you!";
		String host = "smtp.gmail.com";

		// getting system properties
		Properties properties = System.getProperties();

		// setting up properties for mail

		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enabled", "true");

		Session session = Session.getInstance(properties, new Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(from, "oncore@123");
			}

		});

		// for debug
		session.setDebug(true);

		// prepare message

		try {
			MimeMessage msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress(from));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			msg.setSubject(subject);
			msg.setText(message);

			// send mail
			Transport.send(msg);

			System.out.println("Mail Successfully sent");

		} catch (Exception e) {
			System.out.println("Exception occur while sending mail :" + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	public Address[] getAllEmails(){
		
		Address[] emails = new Address[5];
		try {
			emails[0] = new InternetAddress("subashtharu116@gmail.com");
			emails[1] = new InternetAddress("pramish.thapa17@gmail.com");
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		return emails;
	}
}
