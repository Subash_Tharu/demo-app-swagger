package com.oncore.demoappswagger.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api(description = "Api for the home controller")

@RestController
@RequestMapping("/api")
public class HomeController {

	@GetMapping("/hello")
	public String hello() {
		return "hello world";
	}
	
	@PostMapping("/name")
	public String getting(@RequestParam String name) {
		
		return "hellow "+ name;
	}
	
}
